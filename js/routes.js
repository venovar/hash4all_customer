"use strict";
application
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) 
    {
        if(localStorage.getItem('account'))
            $urlRouterProvider.otherwise('/app/dashboard/show');
        else
            $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('404', {
                views : {
                    'content@' : {
                        templateUrl     : 'views/404.html?v=' + Math.random() * 10,
                    }
                },
                url     : '/404',
            })
            .state('login', {
                views : {
                    'content@' : {
                        templateUrl     : 'views/login.html?v=' + Math.random() * 10,
                        controller      : 'LoginController',
                    }
                },
                url     : '/?code',
            })
            .state('forgot', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/forgot.html?v=' + Math.random() * 10,
                        controller      : 'ForgotController'
                    },
                },
                url     : '/forgot',
            })
            .state('signup', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/signup.html?v=' + Math.random() * 10,
                        controller      : 'SignupController'
                    },
                },
                url     : '/signup',
            })
            .state('verify', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/verify.html?v=' + Math.random() * 10,
                        controller      : 'VerifyController'
                    },
                },
                url     : '/verify/:token',
            })
            .state('reset', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/reset.html?v=' + Math.random() * 10,
                        controller      : 'ResetController'
                    },
                },
                url     : '/reset/:token',
            })
            .state('dashboardShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/dashboard/show.html?v=' + Math.random() * 10,
                        controller      : 'DashboardShowController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/dashboard/show',
            })
            /// BUY
            .state('orderStoreStep01', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/order/store/step01.html?v=' + Math.random() * 10,
                        controller      : 'StoreStep01Controller'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/order/store/step01',
                params : {
                    plan : null,
                    amount : null,
                    coin : null,
                    terahash : null,
                }
            })
            .state('orderStoreStep02', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/order/store/step02.html?v=' + Math.random() * 10,
                        controller      : 'StoreStep02Controller'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/order/store/step02',
                params : {
                    plan : null,
                    amount : null,
                    coin : null,
                    terahash : null,
                }
            })
            .state('aboutShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/about/show.html?v=' + Math.random() * 10,
                        controller      : 'AboutShowController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/about/show',
            })
            /// CONTA
            .state('accountShow', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/account/show.html?v=' + Math.random() * 10,
                        controller      : 'AccountShowController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/account/show',
            })
            .state('accountUpdate', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/account/update.html?v=' + Math.random() * 10,
                        controller      : 'AccountUpdateController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/account/update',
            })
            .state('accountPassword', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/account/password.html?v=' + Math.random() * 10,
                        controller      : 'AccountPasswordController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/account/password',
            })
            /// Balance
            .state('balanceIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/balance/index.html?v=' + Math.random() * 10,
                        controller      : 'BalanceIndexController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/balance/index',
            })
            /// History
            .state('orderIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/order/index.html?v=' + Math.random() * 10,
                        controller      : 'OrderIndexController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/order/index',
            })
            /// Share
            .state('shareIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random() * 10,
                    },
                    'content@' : {
                        templateUrl     : 'views/app/share/index.html?v=' + Math.random() * 10,
                        controller      : 'ShareIndexController'
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/app/share/index',
            })
        ;
    });