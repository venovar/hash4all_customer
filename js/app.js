"use strict";
var application = angular
    .module('Application', [
        'ngMaterial', 
        'ngMessages', 
        'ngSanitize', 
        'ngRoute',
        'ui.router',
        'ui.mask',
        'ngAnimate',
        'pascalprecht.translate',
        'chart.js',
        '720kb.socialshare',
    ]);