"use strict";
application
	.directive('dateFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				var elementoMask;
				if(angular.element(element).find('input').length)
		        	elementoMask = angular.element(element).find('input');
		        else 
		        	elementoMask = angular.element(element);
				elementoMask.mask('00/00/0000');
			}
		}
	})
	.directive('cardDateFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				angular.element(element).mask('00/0000');
			}
		}
	})
	.directive('moneyFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				if(!attrs.moneyFormat) {
					attrs.moneyFormat = 'R$';
				}
				/// IN
				ngModelController.$formatters.push(function(data) {
					if(data) {
						return money(data, attrs.moneyFormat);
					}
					return data;
				});
				/// MASK
	        	var option = {
	        		allowNegative	: true,
	        		affixesStay		: false,
	        	};
	        	switch(attrs.moneyFormat) {
	        		case 'R$':
	        			option.precision = 2;
	        			option.thousands = '.';
	        			option.decimal = ',';
	        			break
	        		case '$':
	        			option.precision = 2;
	        			option.thousands = ',';
	        			option.decimal = '.';
	        			break;
	        		case 'BTC':
	        			option.precision = 8;
	        			option.thousands = ',';
	        			option.decimal = '.';
		        		break;
	        	}
	        	option.prefix = attrs.moneyFormat;
	        	
				if(attrs.moneyFormat && attrs.moneyFormat.length) {
					option.prefix = attrs.moneyFormat + ' ';
				}
				angular
		        	.element(element)
		        	.maskMoney(option);

		        /// OUT
				ngModelController.$parsers.push(function(data) {
	    			if(data) {
	    				return unMoney(data, attrs.moneyFormat);
	    			}
	    			return data;
	  			});
			}
		}
	})
	.directive('phoneFormat', function() {
		return {
			require: 'ngModel',
			replace: true,
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						data = data.replace(/\D/g, '');
						if(data.length > 10) {
							return data.replace(/(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
						} else {
							return data.replace(/(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
						}
					}
					return data;
				});
				// Mask
				var brazilMask = function (val) {
					return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
				};
				var internationalMask = function (val) {
					return '000000000000000';
				};
				var options = {
					onKeyPress: function(val, e, field, options) {
						field.mask(brazilMask.apply({}, arguments), options);
					}
				};

				attrs.$observe('phoneFormat', function(value) {
					if(!value || value == 30) {
						angular.element(element).mask(brazilMask, options);
					} else {
						angular.element(element).mask(internationalMask);
					}
				});
				

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.replace(/\D/g, '');
	    			return data;
	  			});
			}
			}
	})
	.directive('plateFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						return data.replace(/(\w{3})(\d{4})/, '$1-$2');
					}
					return data;
				});

				// Mask
				angular.element(element).mask('AAA-0000', {
					translation : {
						'A' : {
							pattern : /[a-zA-Z]/,
						}
					},
					onKeyPress : function (value, event) {
						event.currentTarget.value = value.toUpperCase();
					}
				});

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.toString().replace('-', '');
	    			return data;
	  			});
			}
		}
	})
	.directive('cnpjFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						return cnpj(data);
					}
					return data;
				});

				// Mask
		        angular.element(element).mask('00.000.000/0000-00');

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.replace(/\D/g, '');
	    			return data;
	  			});
			}
			}
	})
	.directive('cpfFormat', function($rootScope) {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						data = data.replace(/\D/g, '');
						return data.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
					}
					return data;
				});

				// Mask
				
	        	angular.element(element).mask('000.000.000-00', {
	        		onComplete : function(cpf) {
	        			$rootScope.$broadcast('cpf', cpf.replace(/\D/g, ''));
	        		}
	        	});

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.replace(/\D/g, '');
	    			return data;
	  			});
			}
		}
	})
	.directive('cardNumberFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						data = data.replace(/\D/g, '');
						return data.replace(/(\d{4})(\d{4})(\d{4})(\d{4})/, '$1 $2 $3 $4');
					}
					return data;
				});

				// Mask
				angular.element(element).mask('0000 0000 0000 0000');

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.replace(/\D/g, '');
	    			return data;
	  			});
			}
		}
	})
	.directive('numberFormat', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data)
						return data.toString().replace(/\D/g, '');
					return data;
				});

				// Mask
				var maskValue = '000000';
				
				if(attrs.numberFormat) {
					maskValue = '';
					for (var i = 0; i < attrs.numberFormat; i++) {
						maskValue += '0';
					}
				}
				
				angular.element(element).mask(maskValue);

				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.toString().replace(/\D/g, '');
	    			return data;
	  			});
			}
		}
	})
	.directive('cepFormat', function($http, $rootScope, $q, $timeout) {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModelController) {
				// Convert model to view
				ngModelController.$formatters.push(function(data) {
					if(data) {
						data = data.replace(/\D/g, '');
						return data.replace(/(\d{5})(\d{3})/, '$1-$2');
					}
					return data;
				});

				// Mask
				angular.element(element).mask('00000-000', {
	        		onComplete : function(cep) {
	        			getAddress(cep.replace(/\D/g, ''));
	        		}
	        	});

		        /*angular.element(element).focusout(function(event) {*/
		        function getAddress (value) {
		        	if(value.length == 8) {
		        		$rootScope.inBackground = true;
		        		var canceler = $q.defer();
						var resolveTimeout = $timeout(canceler.resolve, 15000);
			        	$http({
							method: 'GET',
							url: 'https://viacep.com.br/ws/' + value + '/json/',
							timeout : canceler.promise,
						})
						.then(function(response){
							$rootScope.inBackground = false;
							$timeout.cancel(resolveTimeout);
							if(response.status == 200) {
								var addressElement = element
									.parents('form')
									.find('input[name="address"]');
								addressElement.val(response.data.logradouro);
								addressElement.change();
								addressElement.blur();
								var neighborElement = element
									.parents('form')
									.find('input[name="neighborhood"]');
								neighborElement.val(response.data.bairro);
								neighborElement.change();
								neighborElement.blur();
								var cityElement = element
									.parents('form')
									.find('input[name="city"]');
								cityElement.val(response.data.localidade);
								cityElement.change();
								cityElement.blur();
								var ufElement = element
									.parents('form')
									.find('input[name="uf"], input[name="state"]');
								ufElement.val(response.data.uf);
								ufElement.change();
								ufElement.blur();
							}
						});
		        	}
		        };
				// Convert view to model
				ngModelController.$parsers.push(function(data) {
	    			if(data)
	      				data = data.replace(/\D/g, '');
	    			return data;
	  			});
			}
			}
	})
	.directive('fill', function($window) {
		return {
			restrict: 'A',
			link: function(scope, element) {
				function getComputedStyle (element, styleProp) {
					var r = '';
					if (element.currentStyle) {
						r = element.currentStyle[styleProp];
					} else if ($window.getComputedStyle) {
						r = $window.getComputedStyle(element,null).getPropertyValue(styleProp);
					}        
					return r;
				}
				function getComputedSize(element, styleProp) {
					var r =  getComputedStyle (element, styleProp);
					if (r.indexOf('px')>0) r = r.substring(0,r.length-2);
					return r|0;
				}
				function getRestHeights (element) {
					var before = true;
					var total = 0;
					var pChildren = element.parentNode.childNodes;
					for (var i = 0;i<=pChildren.length;i++) {
						if (before) before = pChildren[i]!=element;
						else {              
							if (pChildren[i]) {
								if (pChildren[i].nodeType==1) 
									getComputedStyle(pChildren[i],'display');
								if (
									pChildren[i].nodeType==1 
									&& getComputedStyle(pChildren[i],'display')!='none'
									&& getComputedStyle(pChildren[i],'position')!='absolute' 
									)
									total += pChildren[i].scrollHeight|0;
							}
						}
					}
					return total;
				}
				function calcBottomPaddings(element) {
					var total = 0;
					total += getComputedSize(element,'padding-bottom');
					total += getComputedSize(element,'margin-bottom');        
					if (element.parentNode && element.parentNode.nodeType == 1) {
						total += calcBottomPaddings(element.parentNode);
						total += getRestHeights(element);
					}
					return total;        
				}
				function applySize() {
					var height = 0;
					var body = window.document.body;
					if (window.innerHeight) {
						height = window.innerHeight;
					} else if (body.parentElement.clientHeight) {
						height = body.parentElement.clientHeight;
					} else if (body && body.clientHeight) {
						height = body.clientHeight;
					}
					var paddings = calcBottomPaddings(element[0]);
					var resultHeight =  (height - element[0].offsetTop - paddings);
					element[0].style.height = resultHeight + "px";
				}
				angular.element($window).bind('resize', applySize);
				var intervalCount = 1;
				var interval = setInterval(function(){
					applySize();
					if(intervalCount == 3)
						clearInterval(interval);
					intervalCount++;
				}, 300);
				applySize();
				scope.$on('destroy', function() {
					angular.element($window).unbind('resize', applySize);
				})
			}
		}
	})
	.directive('whenScrolled', function() {
	    return function(scope, elm, attr) {
	        var raw = elm[0];        
	        elm.bind('scroll', function() {
	            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight - 10) {
	                scope.$apply(attr.whenScrolled);
	            }
	        });
	    };
	})
	.directive('subHeaderBlur', function() {
	    return function(scope, elm, attr) {
	        var raw = elm[0];
	    	angular.element('input, textarea').focusin(function(){
	    		var inputFocus = angular.element(this);
		        function scroll () {
		        	if(inputFocus.offset().top < angular.element('.top-toolbar').height() + inputFocus.height())
		        		angular.element('input, textarea').blur();
		        }
		    	inputFocus.focusout(function(){
		    		elm.unbind('scroll', scroll);
		    	});
		        elm.bind('scroll', scroll);
	    	});
	    };
	})
	.directive('displayAfterFocus', function($timeout) {
	    return function(scope, elm, attr) {
	    	var input;
			var startHeight;
			var resizePromise;
			var container;
			var startScrollTop;
	    	function resizing () {
	    		container = angular.element('body');
				resizePromise = $timeout(function() {
					input.off('change keyup');
					if(input.offset().top > angular.element('body').height()) {
						angular.element('md-content[md-scroll-y]').animate({
							scrollTop : startScrollTop + (startHeight - container.height()) + 20
						});
					}
				}, 1500);
	    	}
	        angular.element('input, textarea').focus(function(event) {
				input = angular.element(this);
				startHeight = angular.element('body').height();
				startScrollTop = angular.element('md-content[md-scroll-y]')[0].scrollTop - angular.element('md-content[md-scroll-y]').offset().top;
				resizing();
				input.on('change keyup', function() {
					$timeout.cancel(resizePromise);
				});
			});
	    };
	})
	.directive('onLongPress', function($timeout) {
		return {
			restrict: 'A',
			link: function($scope, $elm, $attrs) {
				$elm.bind('touchstart', function(evt) {
					// Locally scoped variable that will keep track of the long press
					$scope.longPress = true;

					// We'll set a timeout for 600 ms for a long press
					$timeout(function() {
						if ($scope.longPress) {
							// If the touchend event hasn't fired,
							// apply the function given in on the element's on-long-press attribute
							$scope.$apply(function() {
								$scope.$eval($attrs.onLongPress)
							});
						}
					}, 600);
				});

				$elm.bind('touchend', function(evt) {
					// Prevent the onLongPress event from firing
					$scope.longPress = false;
					// If there is an on-touch-end function attached to this element, apply it
					if ($attrs.onTouchEnd) {
						$scope.$apply(function() {
							$scope.$eval($attrs.onTouchEnd)
						});
					}
				});
			}
		};
	})
	.directive('isLanguage', function() {
		return {
			restrict: 'A',
			link: function(rootScope, scope, element, attrs) {
				if(rootScope.language != element.isLanguage) {
					angular.element(element.$$element[0]).hide();
				}
			}
		}
	})
	.directive('uploadImage', function($http, $rootScope, config) {
		return {
			restrict: 'E',
			scope: {
	            startUpload : '&startUpload',
	            error 	: '&error',
	            success : '&success',
	        },
	        replace 	: true,
	        
	        template 	: 	'<div layout="row" layout-align="start center" ng-disabled="isDisabled">' +
		        				'<input class="ng-hide" id="fileInput" type="file" />' +
								'<label id="uploadButton" for="fileInput" class="md-button">' +
									'<md-icon class="mdi mdi-camera"></md-icon>' +
				                    ' {{ buttonLabel }} ' +
				                '</label>' +
				            '</div>',
						
			link: function($scope, $element, $attrs) {
				var fileInput 	= $element.find('#fileInput');
				var button 		= $element.find('#uploadButton');
				$scope.buttonLabel = '';

		  		fileInput.on('change', function(event) {
		    		if (event.target.files && event.target.files.length) {
		    			var formData = new FormData();
					    formData.append("photo", event.target.files[0]);
					    $scope.isDisabled = true;
					    $rootScope.blockErrorDialog = true;
					    button.removeClass('md-accent');
					    button.removeClass('md-warn');
						if($scope.startUpload()) {
							$scope.startUpload()();
						}
						
						$scope.buttonLabel = "(" + zeroPad(event.target.files.length, 2) + ")";
					    $http({
							headers : {
								'Accept' 		: 'application/json',
								'Authorization' : localStorage.getItem('authorization'),
								'Content-Type'	: undefined 
							},
							method  			: 'POST',
							url 				: config.service_url + $attrs.endPoint,
							transformRequest 	: angular.identity,
							data 				: formData,
						}).then(function(response) {
							if(response.status == 200) {
								if($scope.success()) {
									$scope.success()(response.data);
								}
								button.addClass('md-accent');
							} else {
								if($scope.error()) {
									$scope.error()(crackMessages(response.data));
								}
								button.addClass('md-warn');
							}
							$rootScope.blockErrorDialog = false;
							$scope.isDisabled = false;
						});
					}
		    		$scope.$apply();
		  		});
			},
		};
	})
;
