function parsePhone (phone) {
	if(!phone || phone == '')
		return {};
	return {
		ddd : phone.slice(0,2),
		number : phone.slice(2,12)
	}
}

function fillPad (value, extension, width, left) {
	if(value == undefined || extension == undefined) {
		return extension;
	}
	if(left == undefined)
		left = true;

	var width = width ? width : 1;
	var z = extension;
	var n = value + '';
	if(left)
		return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	else
		return n.length >= width ? n : n + new Array(width - n.length + 1).join(z);
};

function zeroPad (value, width) {
	return fillPad(value, '0', width, true);
};

function money (value, coin) {
    
    if(!value || isNaN(value))
    	return value;
    
    if(coin) {
	    if(coin == '$') {
	    	var 
			n = value, 
		    c = 2,
		    d = '.',
		    t = ',',
		    s = n < 0 ? "-" : "", 
		    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(-c) : "");
	    } else if(coin == 'BTC') {
	    	var 
			n = value, 
		    c = 8,
		    d = '.',
		    t = ',',
		    s = n < 0 ? "-" : "", 
		    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(-c) : "");
	    } 
    }
    
    var 
	n = value, 
    c = 2,
    d = ',',
    t = '.',
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(-c) : "");
}

function unMoney (value, coin) {
    
    value = value
    	.replace('R$', '')
    	.replace('$', '')
    	.replace('BTC', '')
    	.trim();
    
    if(coin && (coin == '$' || coin == 'BTC')) {
	    value = value.replace(',', '');
    } else {
		value = value.replace('.', '');
		value = value.replace(',', '.');
    }
    
	return value;
}

function emailValidate(mail)
{  
	return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail));
}  

function cpf (value) {
	if (!value) { return ''; }
	value = value.replace(/\D/g, '');
	return value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
}

function cnpj (value) {
	if (!value) { return ''; }
	value = value.replace(/\D/g, '');
	return value.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
}

function arrayColumn (values, column) {
	if(column && values && values.length) {
		var result = [];
		for(var prop in values) {
			result.push(values[prop][column]);
		}
		return result;
	}
	return values;
}

function crackMessages(o, indent) {
	var out = [];
    if (typeof indent === 'undefined') {
        indent = 0;
    }
    for (var p in o) {
        if (o.hasOwnProperty(p)) {
            var val = o[p];
            if (typeof val === 'object') {
				out.push( crackMessages(val, indent + 1) + new Array(4 * indent + 1).join(' ') );
            } else {
                out.push( val );
            }
        }
    }
    return out.join('<BR>');
}

function getItemFromArray (list, value, attr) {
	if(!attr) {
		attr = 'id';
	}
	for(var prop in list) {
		if(list[prop][attr] == value) {
			return list[prop];
		}
	}
	return null;
}