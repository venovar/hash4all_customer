"use strict";
application
	.filter('tel', function () {
		return function (tel, params) {
			
			if (!tel) { return ''; }
			tel = tel.replace(/\D/g, '');
			
			if(params == 30) {
				if(tel.length > 10) {
					return tel.replace(/(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
				} else {
					return tel.replace(/(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
				}
			}
			
			return tel;
		};
	})
	/// 11.655.744/0001-60
	.filter('cnpj', function () {
		return function (value) {
			return cnpj(value);
		};
	})
	/// 055.371.876-20
	.filter('cpf', function () {
		return function (value) {
			return cpf(value);
		};
	})
	/// 04619-000
	.filter('cep', function () {
		return function (cep) {
			if (!cep) { return ''; }
			cep = cep.replace(/\D/g, '');
			return cep.replace(/(\d{5})(\d{3})/, '$1-$2');
		};
	})
	/// Zero a esquerda.
	.filter('zeroPad', function () {
		return zeroPad;
	})
	.filter('cardNumber', function () {
		return function (value) {
			if(value < 16) {
				var width = 16;
				var z = '•';
				var n = value + '';
				var result = n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
				return result.replace(/(\•{4})(\•{4})(\•{4})(\d{4})/, '$1 $2 $3 $4');
			} else {
				return value.replace(/(\d{4})(\d{4})(\d{4})(\d{4})/, '$1 $2 $3 $4');
			}
		};
	})
	.filter('datetime', function ($filter) {
		return function (value, params) {
			if(!value)
				return '';
			if(!params) {
				if(value.length <= 10)
					params = 'DD/MM/YYYY';
				else
					params = 'DD/MM/YYYY HH:mm:ss';
			}
			moment.locale('pt-BR');
			return moment(value, "YYYY-MM-DD H:m:s").format(params);
		};
	})
	.filter('plate', function () {
		return function (code) {
			if (!code) { return ''; }
			return code.replace(/(\w{3})(\d{4})/, '$1-$2');
		};
	})
	.filter('service_url', function (config) {
		return function (value) {
			if (!value) { return ''; }
			return config.service_url + '/' + value;
		};
	})
	.filter('money', function () {
		return function (value, coin) {
			return money(value, coin);
		};
	})
	.filter('age', function(){
	    return function(birthday){
	    	if(birthday) {
		        var birthday = new Date(birthday);
		        var today = new Date();
		        var age = ((today - birthday) / (31557600000));
		        var age = Math.floor( age );
	        	return age;
	    	}
	    	return birthday;
	    }
	})
	.filter('url', function(config){
	    return function(value){
	    	return config.service_url + '/' + value;
	    }
	});