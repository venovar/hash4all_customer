"use strict";
application
	.controller('LoginController', function (
		$q,
		$http,
		$state,
		$scope,
		$window,
		$timeout,
		$mdDialog,
		$rootScope,
		$translate,
		$stateParams,
		config) 
	{	
		$scope.social = function(target) {
			localStorage.setItem('originRedirect', target);
			
			$http({
					url 	: config.service_url + '/api/v1/' + $rootScope.language + '/oauth/social',
					method  : 'POST',
					headers : {
						'Accept' : 'application/json',
					},
					data : {
						target : target
					}
				}).then(function(response) {
					if(response.status == 200) {
						if(response.data.redirect != '') {
							$window.location.href = response.data.redirect;
						}
					}
				});
		};
		
		function returnFromSocial () {
			if($stateParams.code && $stateParams.code != '' && localStorage.getItem('originRedirect')) {
				$http({
						url 	: config.service_url + '/api/v1/' + $rootScope.language + '/oauth/social',
						method  : 'POST',
						headers : {
							'Accept' : 'application/json',
						},
						data : {
							code : $stateParams.code,
							target : localStorage.getItem('originRedirect'),
						}
					}).then(function(response) {
						if(response.status == 200) {
							localStorage.setItem('authorization', response.data.token_type + ' ' + response.data.access_token);					
							localStorage.setItem('username', response.data.username);
							$rootScope.accountShow(function() {
								$state.go('dashboardShow');
							});
						} else {
							alert(response.data.message, function() {
								$scope.login(event, data.account);
							}, $translate.instant('Warn'), 'OK');
						}
						localStorage.removeItem('originRedirect');
					});
			}
		}
		
		returnFromSocial();
		
		
		$scope.login = function(event, account) {
			
			$mdDialog.show({
                templateUrl: '/views/dialog/login.html?v=1',
                parent		: angular.element(document.body),
                targetEvent	: event,
                clickOutsideToClose	: true,
                fullscreen	: false,
                controller 	: loginDialogController,
                locals : {
                	account : account
                }
            })
            .then(function(data) {
            	if(data && data.action == 'forgot') {
            		$state.go('forgot');
            	} else if(data && data.action == 'login') {
            		
					if(!data.account.username || data.account.username.trim().length <= 4 || !emailValidate(data.account.username.trim())) {
						angular.element('[ng-model="username"]').focus();
						return alert($translate.instant('E-mail invalid.'), function() {
							$scope.login(event, data.account);
						}, $translate.instant('Warn'), 'OK');
					}
					if(!data.account.password || data.account.password.trim().length < 1) {
						angular.element('[ng-model="password"]').focus();
						return alert($translate.instant('Password is empty.'), function() {
							$scope.login(event, data.account);
						}, $translate.instant('Warn'), 'OK');
					}
					if(data.account.password.trim().length < 4) {
						angular.element('[ng-model="password"]').focus();
						return alert($translate.instant('Password must contain 4 numbers.'), function() {
							$scope.login(event, data.account);
						}, $translate.instant('Warn'), 'OK');
					}
					
					$rootScope.blockShowError = true;
					$http({
						url 	: config.service_url + '/api/v1/' + $rootScope.language + '/oauth/token',
						method  : 'POST',
						data 	: {
							grant_type		: config.grant_type,
							client_id		: config.client_id,
							client_secret	: config.client_secret,
							username		: data.account.username,
							password		: data.account.password,
						},
						headers : {
							'Accept' : 'application/json',
						}
					}).then(function(response) {
						if(response.status == 200) {
							localStorage.setItem('authorization', response.data.token_type + ' ' + response.data.access_token);					
							localStorage.setItem('username', data.account.username);
							$rootScope.accountShow(function() {
								$state.go('dashboardShow');
							});
						} else {
							alert(response.data.message, function() {
								$scope.login(event, data.account);
							}, $translate.instant('Warn'), 'OK');
						}
						
						$rootScope.blockShowError = false;
					});
            	}
            });
		}
		
		function loginDialogController ($mdDialog, $scope, account) {
			
			$scope.account = account ? account : {
				username : localStorage.getItem('username')
			};
			
			$scope.cancel = function () {
				$mdDialog.cancel();
			}
			$scope.forgot = function () {
				$mdDialog.hide({ action : 'forgot' });
			}
			$scope.submit = function () {
				$mdDialog.hide({ action : 'login', account : $scope.account });
			}
		}
		
		$timeout(function(){
			angular.element('.login-page').addClass('login-page-enabled');
		}, 300);
	})
;