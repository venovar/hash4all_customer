"use strict";
application
	.controller('ResetController', function (
		$http,
		$state,
		$scope,
		$stateParams,
		$rootScope,
		$translate,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Reset Password');
		$rootScope.backPageTo('login');
		$scope.submit = function () {
			
			if(!$scope.password || $scope.password.trim().length < 1) {
				angular.element('[ng-model="password"]').focus();
				return alert($translate.instant('Set your new password.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.password.trim().length < 4) {
				angular.element('[ng-model="password"]').focus();
				return alert($translate.instant('Password must contain 4 numbers.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.password != $scope.password_confirmation) {
				angular.element('[ng-model="password"]').focus();
				return alert($translate.instant('Passwords do not match.'), null, $translate.instant('Warn'), 'OK');
			}
			
			$http({
				headers : {
					'Accept' : 'application/json'
				},
				method  : 'POST',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/account/reset',
				data 	: {
					token : $stateParams.token,
					password : $scope.password,
					password_confirmation : $scope.password_confirmation,
				},
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('login');
					}, $translate.instant('SUCCESS'));
				}
			});
		}
	})
;