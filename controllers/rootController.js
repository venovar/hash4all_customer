"use strict";
application
	.controller('RootController', function (
		$http,
		$state,
		$window,
		$timeout,
		$mdDialog,
		$mdSidenav,
		$rootScope,
		$translate,
		config) 
	{		
		var requestResponseStack = [];
		var accountJson = localStorage.getItem('account');
		
		if(localStorage.getItem('language')) {
			$rootScope.language = localStorage.getItem('language');
			$translate.use(localStorage.getItem('language'));
		} else {
			$rootScope.language = 'pt_br';
			$translate.use('pt_br');
		}
		
		moment.locale('pt-BR');
		FastClick.attach(document.body);

		$rootScope.$on('loading', function(event, flag) 
		{
			/// Bloqueia o loading geral. Geralmente para ser utilizado o loading do scroll.
			if(!$rootScope.inBackground)
				$rootScope.loading = flag;
		});
		$rootScope.toggleMenu = function() 
		{
			$mdSidenav('left').toggle();
		};

		$rootScope.logout = function() 
		{
			$rootScope.inBackground = true;
			localStorage.removeItem('account');
			$state.go("login");
		};
		
		if(!(accountJson === undefined || accountJson === null || accountJson == 'undefined')) {
			$rootScope.account = JSON.parse(accountJson);
		}

		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) 
		{
			$rootScope.$on('$stateChangeStart', stateChangeStartEvent);
		});
		$rootScope.$on('$stateChangeStart', stateChangeStartEvent);
		function stateChangeStartEvent (event, toState, toParams, fromState, fromParams) {
			delete $rootScope.navigationTitle;
			delete $rootScope.navigationRightButton;
			delete $rootScope.navigationRightValue
			delete $rootScope.inBackground;
			requestResponseStack = [];
			delete $rootScope.backPage;
			/// Remove todos os ouvintes de error, pra não ser disparado em outras telas.
			$rootScope.$offAll('error');
			$rootScope.$on('error', error);
			angular.element('input, textarea').blur();
			$rootScope.$offAll('$stateChangeStart');
		}
		
		$rootScope.backPageTo = function (to, params)
		{
			$rootScope.backPage = function ()
			{
				angular.element('.content-container > div').removeClass('rtl');
				angular.element('.content-container > div').addClass('ltr');
				$timeout(function() { $state.go(to, params); }, 1);
			};
		};
		
		/// Quando não houver permissão.
		$rootScope.$on('unauthorized', function(event, data, status) 
		{
			requestResponseStack = [];
			
			if(!$rootScope.blockShowError) {
				localStorage.removeItem('account');
				
				var message = !data.message || data.message == '' ? $translate.instant('You don`t have permission.') : $translate.instant(data.message);
				
				alert(message, function() {
					$state.go('login');
				}, $translate.instant('Warn'), 'OK');
			}
			
			$rootScope.$broadcast('loading', false);
		});
		$rootScope.$on('request', function() 
		{
			requestResponseStack.push('');
			$rootScope.$broadcast('loading', true);
		});
		$rootScope.$on('response', function() 
		{
			requestResponseStack.pop();
			if(!requestResponseStack || !requestResponseStack.length)
				$rootScope.$broadcast('loading', false);
		});
		
		function error (event, data, status) 
		{
			console.log(data);
			requestResponseStack = [];
			if(!$rootScope.blockShowError) {
				if(status == 400) 
				{	
					alert(crackMessages(data.errors), null, $translate.instant('Warn'), 'OK');
				} 
				else if(status == 0) 
				{
					alert($translate.instant('There was a problem. Check your connection.'), null, $translate.instant('Warn'), 'OK');
				} 
				else if(!data)
				{
					var message = data && data.message && data.message != '' ? data.message : $translate.instant('There was a problem. Contact us.');
					alert(message, null, $translate.instant('Warn'), 'OK');
				}
			}
			$rootScope.$broadcast('loading', false);
		};
		
		$rootScope.accountShow = function (callBack) {
			$http({
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/show',
				method  : 'GET',
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				}
			}).then(function(response) {
				if(response.status == 200) {
					$rootScope.account = response.data.user;
					localStorage.setItem('account', JSON.stringify($rootScope.account));
					if(callBack)
						callBack();
				}
			});
		}
		
		window.alert = function(message, resultFunction, title, buttonLabel) {
			var dialogShow;
			var alertDialog = $mdDialog.alert();
			alertDialog.parent(angular.element( document.body ));
			alertDialog.clickOutsideToClose(false);
			alertDialog.htmlContent(message);
			if(!title)
				title = $translate.instant('Message');
			alertDialog.title(title);
			alertDialog.ariaLabel(title);
			if(!buttonLabel)
				buttonLabel = 'OK';
			alertDialog.ok(buttonLabel);
			dialogShow = $mdDialog.show(alertDialog);
			if(resultFunction) {
				dialogShow.then(resultFunction);
			}
		};
		window.confirm = function(message, successFunction, cancelFunction, title, okLabel, cancelLabel) {
			var dialogShow;
			var confirmDialog = $mdDialog.confirm();
			confirmDialog.parent(angular.element( document.body ));
			confirmDialog.clickOutsideToClose(false);
			confirmDialog.textContent(message);
			if(!title)
				title = $translate.instant('Message');
			confirmDialog.title(title);
			confirmDialog.ariaLabel(title);
			if(!okLabel)
				okLabel = 'OK';
			confirmDialog.ok(okLabel);
			if(!cancelLabel)
				cancelLabel = $translate.instant('Cancel');
			confirmDialog.cancel(cancelLabel);
			dialogShow = $mdDialog.show(confirmDialog);
			if(successFunction || cancelFunction) {
				dialogShow.then(successFunction, cancelFunction);
			}
		};
		window.prompt = function(message, successFunction, cancelFunction, title, okLabel, cancelLabel) {
			var dialogShow;
			var promptDialog = $mdDialog.prompt();
			promptDialog.parent(angular.element( document.body ));
			promptDialog.clickOutsideToClose(false);
			promptDialog.textContent(message);
			if(!title)
				title = $translate.instant('Message');
			promptDialog.title(title);
			promptDialog.ariaLabel(title);
			if(!okLabel)
				okLabel = 'OK';
			promptDialog.ok(okLabel);
			if(!cancelLabel)
				cancelLabel = $translate.instant('Cancel');
			promptDialog.cancel(cancelLabel);
			dialogShow = $mdDialog.show(promptDialog);
			if(successFunction || cancelFunction) {
				dialogShow.then(successFunction, cancelFunction);
			}
		};
	})
	.run(function($rootScope, $state, $window){
	    $rootScope.$off = function (name, listener) {
	        var namedListeners = this.$$listeners[name];
	        if(namedListeners) {
	            for (var i = 0; i < namedListeners.length; i++) {
	                if(namedListeners[i] === listener) {
	                    return namedListeners.splice(i, 1);
	                }
	            }
	        }
	    }
	    $rootScope.$offAll = function (name) {
	        delete this.$$listeners[name];
	    }
	})
;