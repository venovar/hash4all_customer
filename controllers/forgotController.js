"use strict";
application
	.controller('ForgotController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Forgot');
		$rootScope.backPageTo('login');
		$scope.submit = function () {
			
			if(!$scope.username || $scope.username.trim().length <= 4 || !emailValidate($scope.username.trim())) {
				angular.element('[ng-model="username"]').focus();
				return alert($translate.instant('E-mail invalid.'), null, $translate.instant('Warn'), 'OK');
			}
			
			$http({
				headers : {
					'Accept' : 'application/json'
				},
				method  : 'POST',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/account/forgot',
				data 	: {
					email : $scope.username
				},
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('login');
					}, $translate.instant('Success'));
				}
			});
		}
	})
;