"use strict";
application
	.controller('SignupController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$filter,
		$translate,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Signup');
		$rootScope.backPageTo('login');
		$scope.user = {};

		angular.element('.md-datepicker-input-container input').attr('type', 'tel');
		
		function dataprovider () { 
			
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/guest/customer/dataprovider'
			}).then(function(response) {
				if(response.status == 200) {
					$scope.countries = response.data.countries;
					$scope.user.customer = {
						country_id : 30
					}
					$scope.countryChange(30);
					
				}
			});
		}
		dataprovider();
		
		$scope.countryChange = function (id) {
			$scope.countrySelected = getItemFromArray($scope.countries, id);
		}
		
		$scope.submit = function () 
		{
			angular.element('input').blur();
			if(!$scope.user.name || $scope.user.name.trim().length <= 1) {
				angular.element('[ng-model="user.name"]').focus();
				return alert($translate.instant('This name is invalid.'), null, $translate.instant('Warn'), 'OK');
			}
			if(!$scope.user.email || $scope.user.email.trim().length <= 4 || !emailValidate($scope.user.email.trim())) {
				angular.element('[ng-model="user.email"]').focus();
				return alert($translate.instant('E-mail invalid.'), null, $translate.instant('Warn'), 'OK');
			}
			if(!$scope.user.password || $scope.user.password.trim().length < 1) {
				angular.element('[ng-model="user.password"]').focus();
				return alert($translate.instant('Set your new password.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.user.password.trim().length < 4) {
				angular.element('[ng-model="user.password"]').focus();
				return alert($translate.instant('Password must contain 4 numbers.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.user.password != $scope.user.password_confirmation) {
				angular.element('[ng-model="user.password"]').focus();
				return alert($translate.instant('Passwords do not match.'), null, $translate.instant('Warn'), 'OK');
			}
			
			$http({
				headers : {
					'Accept' : 'application/json'
				},
				method  : 'POST',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/guest/customer/store',
				data 	: $scope.user,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('login');
					}, $translate.instant('Success'));
				}
			});
		}
	})
;