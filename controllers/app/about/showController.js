"use strict";
application
	.controller('AboutShowController', function (
		$http,
		$state,
		$scope,
		$timeout,
		$rootScope,
		$translate,
		$mdDialog,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('About');
	})
;