"use strict";
application
	.controller('DashboardShowController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		$filter,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('My Hashrate');
		function show () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/dashboard/show',
			}).then(function(response) {
				if(response.status == 200) {
					$scope.user = response.data.user;
				}
			});
		}
		show();
		
		$scope.square = angular.element('.dashboard-show').width() - 60;
		
	});