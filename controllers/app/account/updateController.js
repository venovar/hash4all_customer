"use strict";
application
	.controller('AccountUpdateController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		$mdDialog,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Account Customer');
		
		$scope.user = {};
		angular.element('.md-datepicker-input-container input').attr('type', 'tel');
		var initHashRate;
		
		$rootScope.backPageTo('accountShow');
		
		function dataprovider () { 
			
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/dataprovider'
			}).then(function(response) {
				if(response.status == 200) {
					$scope.countries = response.data.countries;
					show();
				}
			});
		}
		dataprovider();
		
		$scope.countryChange = function (id) {
			$scope.countrySelected = getItemFromArray($scope.countries, id);
		}
		
		function show () { 
			
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/show'
			}).then(function(response) {
				if(response.status == 200) {
					if(response.data.user && response.data.user.customer && response.data.user.customer.phone && response.data.user.customer.phone.length > 11 && response.data.user.customer.country) {
						var number = response.data.user.customer.phone.replace(/\D/g, '');
						if(number.indexOf(response.data.user.customer.country.phonecode) == 0) {
							response.data.user.customer.phone = 
								number.replace(response.data.user.customer.country.phonecode, '');
						}
					}
					$scope.user = response.data.user;
					
					initHashRate = angular.copy(response.data.user.customer.terahash);
					
					$scope.countryChange(response.data.user.customer.country_id);
				}
			});
		}
		
		function submitContinue () {
				
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'POST',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/update',
				data 	: $scope.user,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('accountShow');
					}, $translate.instant('Success'));
				}
			});
		}
		
		$scope.submit = function () 
		{
			angular.element('input, textarea').blur();
			
			if(!$scope.user.name || $scope.user.name.trim().length <= 1) {
				angular.element('[ng-model="user.name"]').focus();
				return alert($translate.instant('This name is invalid.'), null, $translate.instant('Warn'), 'OK');
			}
			if(!$scope.user.email || $scope.user.email.trim().length <= 4 || !emailValidate($scope.user.email.trim())) {
				angular.element('[ng-model="user.email"]').focus();
				return alert($translate.instant('E-mail invalid.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.user.password != $scope.user.password_confirmation) {
				angular.element('[ng-model="user.password"]').focus();
				return alert($translate.instant('Passwords do not match.'), null, $translate.instant('Warn'), 'OK');
			}
			
			if(parseInt(initHashRate) == parseInt($scope.user.customer.terahash)) {
				submitContinue();
			} else {
				confirm($translate.instant('Are you sure want to change this Terahash?'),
					submitContinue, 
					null, 
					$translate.instant('Warn')
				);
			}
			
		}
		
		$scope.uploadError = function(message) {
			alert(message, null, $translate.instant('Warn'), 'OK');
		}
		$scope.uploadSuccess = function(data) {
			if(data.photo) {
				$scope.user.avatar 		= [data.photo];
				$scope.user.photo_id 	= data.photo.id;
			}
		}
	})
;