"use strict";
application
	.controller('AccountPasswordController', function (
		$http,
		$scope,
		$translate,
		$rootScope,
		$mdDialog,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Security');
		$rootScope.backPageTo('accountShow');
		
		$scope.submit = function () 
		{
			if(!$scope.password || $scope.password.trim().length < 1) {
				angular.element('[ng-model="password"]').focus();
				return alert($translate.instant('Set your new password.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.password.trim().length < 4) {
				angular.element('[ng-model="password"]').focus();
				return alert($translate.instant('Password must contain 4 numbers.'), null, $translate.instant('Warn'), 'OK');
			}
			if($scope.password != $scope.password_confirmation) {
				angular.element('[ng-model="password"]').focus();
				return alert($translate.instant('Passwords do not match.'), null, $translate.instant('Warn'), 'OK');
			}
			
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'POST',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/password',
				data 	: {
					current_password 		: $scope.current_password,
					password 				: $scope.password,
					password_confirmation 	: $scope.password_confirmation,
				},
			}).then(function(response) {
				if(response.status == 200) {
					delete $scope.current_password;
					delete $scope.password;
					delete $scope.password_confirmation;
					alert(response.data.message, null, 'Sucesso');
				}
			});
		}
	})
;