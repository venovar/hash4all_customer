"use strict";
application
	.controller('AccountShowController', function (
		$http,
		$state,
		$scope,
		$timeout,
		$rootScope,
		$translate,
		$mdDialog,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Account');
		
		$scope.user = $rootScope.account;
		
		$timeout(function(){
			$scope.selectedMode = 'md-scale';
		}, 700);
		
		function show() {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/show'
			}).then(function(response) {
				if(response.status == 200) {
					$scope.user = response.data.user;
					$rootScope.account = response.data.user;
					localStorage.setItem('account', JSON.stringify(response.data.user));
				}
			});
		}
		
		show();
		
		$scope.delete = function () {
			confirm($translate.instant('Do you really want to remove your account?'), function() {
				$http({
					headers : {
						'Accept' : 'application/json',
						'Authorization' : localStorage.getItem('authorization')
					},
					method  : 'DELETE',
					url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/user/destroy'
				}).then(function(response) {
					if(response.status == 200) {
						alert($translate.instant('Account removed.'), function() {
							$rootScope.logout();
						}, $translate.instant('Success'));
					}
				});
			}, null, $translate.instant('Warn'), $translate.instant('Yes'), $translate.instant('No'));
		}
	})
;