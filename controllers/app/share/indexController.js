"use strict";
application
	.controller('ShareIndexController', function (
		$http,
		$scope,
		$translate,
		$rootScope,
		Socialshare,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Invite & Share');
		
		$scope.facebook = function() {
			Socialshare.share({
		      'provider' : 'facebook',
		      'attrs': {
		        'socialshareUrl' 	: 'http://hash4all.com/',
		        'socialshareText' 	: 'I love Hash4️All®.',
		        'socialshareType'	: 'Share',
		        'socialshareVia'	: '1468337583226101',
		        'socialshareQuote'	: 'I love Hash4️All®.',
		        'socialshareHashtags'	: 'Hash4All, Bitcoin',
		      }
		    });
		}
		
		$scope.twitter = function() {
			Socialshare.share({
		      'provider' : 'twitter',
		      'attrs': {
		        'socialshareUrl' 	: 'http://hash4all.com/',
		        'socialshareText' 	: 'I love Hash4️All®.',
		        'socialshareHashtags'	: 'Hash4All, Bitcoin',
		      }
		    });
		}
		
		$scope.linkedin = function() {
			Socialshare.share({
		      'provider' : 'linkedin',
		      'attrs': {
		        'socialshareUrl' 	: 'http://hash4all.com/',
		        'socialshareText' 	: 'I love Hash4️All®.',
		        'socialshareSubreddit'	: 'Hash4All, Bitcoin',
		      }
		    });
		}
		
		$scope.email = function() {
			prompt(
				$translate.instant('Send emails to your friends.'),
				function(message) {
					var emails = message.split(';');
					
					if(emails.length > 0) {
						
						$http({
							headers : {
								'Accept' : 'application/json',
								'Authorization' : localStorage.getItem('authorization')
							},
							method  : 'POST',
							url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/share/email',
							data	: {
								emails : emails
							}
						}).then(function(response) {
							if(response.status == 200) {
								alert(response.data.message, null, 'Sucesso', 'OK');
							}
						});
					}
				},
				null,
				$translate.instant('Share'),
				$translate.instant('Send'),
				$translate.instant('Cancel')
			);
		}
	})
;