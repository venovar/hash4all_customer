"use strict";
application
	.controller('StoreStep01Controller', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		$stateParams,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Buy');
		var itemHistory;
		
		if(localStorage.getItem('cart')) {
			var cart = JSON.parse(localStorage.getItem('cart'));
			for(var prop in cart) {
				$scope[prop] = cart[prop];
			}
			itemHistory = angular.copy($scope.plan);
		} else if($stateParams.plan) {
			localStorage.setItem('cart', JSON.stringify($stateParams));
			for(var prop in $stateParams) {
				$scope[prop] = $stateParams[prop];
			}
			itemHistory = angular.copy($scope.plan);
		} else {
			$scope.amount = 0;
			$scope.terahash = 0;
		}
		
		localStorage.removeItem('cart');
		
		function dataprovider () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/order/dataprovider'
			}).then(function(response) {
				if(response.status == 200) {
					$scope.coins = response.data.coins;
					if(!$scope.coin && $scope.coins.length) {
						$scope.coin = $scope.coins[1];
					} else if($scope.coin) {
						for(var prop in $scope.coins) {
							if($scope.coins[prop].id == $scope.coin.id) {
								$scope.coin = $scope.coins[prop];
								break;
							}
						}
					}
					$scope.plans = response.data.plans;
				}
			});
		}
		dataprovider();
		$scope.selectPlan = function (item) {
			if(!angular.equals(itemHistory, angular.copy(item))) {
				$scope.amount = 0;
				$scope.terahash = 0;
			}
			
			$scope.plan = item;
			$scope.amount++;
			$scope.terahash += item.terahash;
			itemHistory = angular.copy(item);
		}
		
		$scope.reset = function() {
			$scope.amount = 0;
			$scope.terahash = 0;
			$scope.plan = null;
		}
	})
;