"use strict";
application
	.controller('StoreStep02Controller', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		$stateParams,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Checkout');
		
		if(localStorage.getItem('cart')) {
			var cart = JSON.parse(localStorage.getItem('cart'));
			for(var prop in cart) {
				$scope[prop] = cart[prop];
			}
		} else if($stateParams.plan) {
			localStorage.setItem('cart', JSON.stringify($stateParams));
			for(var prop in $stateParams) {
				$scope[prop] = $stateParams[prop];
			}
		} else {
			$state.go('orderStoreStep01');
		}
		
		localStorage.removeItem('cart');
		
		$rootScope.backPageTo('orderStoreStep01', $scope);
		
		$scope.submit = function () {
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'POST',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/order/store',
				data	: {
					plan_id : $scope.plan.id,
					amount : $scope.amount,
					coin_id : $scope.coin.id,
				}
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('orderIndex');
					}, 'Sucesso', 'OK');
				}
			});
		}
	})
;