"use strict";
application
	.controller('OrderIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		$mdDialog,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('History');
		$scope.sort = 'DESC';
		
		$scope.index = function(reset) { 
			if(reset) {
				$scope.page = 1;
				$scope.orders = [];
			}
			
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/order/index',
				params 	: {
					page : $scope.page,
					sort : $scope.sort,
				}
			}).then(function(response) {
				if(response.status == 200) {
					$scope.orders = $scope.orders.concat(response.data.orders.data);
					if(response.data.orders.next_page_url != null) {
						$scope.page++; 
					} else {
						$scope.page = 0;
					}
				}
			});
		}
		
		$scope.index(true);
		
		$scope.switchSort = function () {
			$scope.sort = $scope.sort == 'ASC' ? 'DESC' : 'ASC';
			$scope.index(true);
		}
	});