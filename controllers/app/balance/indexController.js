"use strict";
application
	.controller('BalanceIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$translate,
		$mdDialog,
		config) 
	{
		$rootScope.navigationTitle = $translate.instant('Balance');
		$scope.sort = 'ASC';
		
		$scope.index = function(reset) { 
			if(reset) {
				$scope.page = 1;
				$scope.coin_transactions = [];
			}
			
			$http({
				headers : {
					'Accept' : 'application/json',
					'Authorization' : localStorage.getItem('authorization')
				},
				method  : 'GET',
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/customer/coin_transaction/index',
				params 	: {
					page : $scope.page,
					sort : $scope.sort,
				}
			}).then(function(response) {
				if(response.status == 200) {
					$scope.coin_transactions = $scope.coin_transactions.concat(response.data.coin_transactions.data);
					if(response.data.coin_transactions.next_page_url != null) {
						$scope.page++; 
					} else {
						$scope.page = 0;
					}
				}
			});
		}
		
		$scope.index(true);
		
		$scope.switchSort = function () {
			$scope.sort = $scope.sort == 'ASC' ? 'DESC' : 'ASC';
			$scope.index(true);
		}
	});