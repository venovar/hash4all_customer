"use strict";
application
	.controller('VerifyController', function (
		$http,
		$scope,
		$rootScope,
		$stateParams,
		$translate,
		$state,
		config
	) {
		$rootScope.navigationTitle = $translate.instant('Account Verification');
		$scope.message = $translate.instant('Checking') + '...';
		$rootScope.backPageTo('login');
		
		if($stateParams.token) 
		{
			$http({
				url 	: config.service_url + '/api/v1/' + $rootScope.language + '/account/verify/' + $stateParams.token,
				method  : 'GET',
				headers : {
					'Accept' : 'application/json',
				}
			}).then(function(response) {
				if(response.status == 200) {
		            $scope.message = response.data.message;
				} else {
					$scope.message = $translate.instant('Checked!');
				}
			});
		} else {
			$scope.message = $translate.instant('There was a problem. Contact-us!');
		}
	})
;